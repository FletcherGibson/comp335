/*
create database tenant1;
create table tenant1.movie(
movie_name varchar(20) not null,
movie_id 	varchar(10) not null,
movie_detail varchar(100),
movie_type  varchar(50),
primary key (movie_id)
);

create table tenant1.ticket(
ticket_id varchar(10) primary key not null,
ticket_time time not null,
ticket_date	date not null ,
ticket_seat varchar(10) not null,
price int
);


create table tenant1.client(
client_id varchar(10) primary key not null, 
client_name	varchar(20) not null , 
pwd varchar(20) not null
);
insert into tenant1.client values('C001','Denny','12345678') ;
insert into tenant1.movie values('Coco','M001','Beautiful music, perfect for family watching','Animation');
insert into tenant1.ticket values('T001','01:00:00.','2018-05-10','K2L4',50);

insert into tenant1.client values('C002','Clare','12345678') ;
insert into tenant1.movie values('Life of the Party','M002','Nice watching','Comedy');
insert into tenant1.ticket values('T002','02:00:00.','2018-02-10','K4L4',20);
insert into tenant1.client values('C003','Morse','12345678') ;
insert into tenant1.movie values('Deadpool2','M003','A superhero movie','Fiction');
insert into tenant1.ticket values('T003','14:00:00.','2018-05-19','D2L4',20);
insert into tenant1.client values('C004','Eliz','12345678') ;
insert into tenant1.movie values('Tully','M004','','Drama');
insert into tenant1.ticket values('T004','01:00:00.','2018-05-10','K2L4',50);
*/
