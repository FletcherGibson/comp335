package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class Main extends Application {
	
	/**
	 * Load webpages for interaction
	 */
	@Override
	public void start(Stage stage) {        
		
		String url = "http://127.0.0.1:8080/";	// TODO update to hosted domain
		String appHeader = "CinemaPosSystem";
		
        Scene scene = new Scene(new Group());
        StackPane root = new StackPane();    
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        
        webEngine.setOnAlert(event -> showAlert(event.getData()));
        webEngine.setConfirmHandler(message -> showConfirm(message));
        
        stage.setTitle(appHeader);
        stage.setWidth(1000);
        stage.setHeight(800);

        webEngine.load(url);
        root.getChildren().add(browser); 
        scene.setRoot(root);  
        stage.setScene(scene);
        stage.show();
        
        
  
	}
	
    private void showAlert(String message) {
        Dialog<Void> alert = new Dialog<>();
        alert.getDialogPane().setContentText(message);
        alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
        alert.showAndWait();
    }

    private boolean showConfirm(String message) {
        Dialog<ButtonType> confirm = new Dialog<>();
        confirm.getDialogPane().setContentText(message);
        confirm.getDialogPane().getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        boolean result = confirm.showAndWait().filter(ButtonType.YES::equals).isPresent();

        // for debugging:
        System.out.println(result);

        return result ;
    }
	
	public static void main(String[] args) {
		launch(args);
	}
}
