﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace SelDemo
{
    public static class Browser
    {
        private static string baseUrl = TestData.BaseUrl;

        //Here needs to add as parameter the path of driver
        public static FirefoxProfileManager profile = new FirefoxProfileManager();
        public static FirefoxProfile myProfile = profile.GetProfile("Selenium");

        public static IWebDriver webDriver = new FirefoxDriver(myProfile);



        public static void Initialize()
        {
            webDriver.Manage().Window.Maximize();
            Goto("");
        }

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static ISearchContext Driver
        {
            get { return webDriver; }
        }

        public static void Goto(string url)
        {
            webDriver.Url = baseUrl + url;
        }

        public static void Close()
        {
           webDriver.Close();
           webDriver.Dispose();
        }
    }
}