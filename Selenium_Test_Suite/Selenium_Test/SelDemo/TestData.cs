﻿namespace SelDemo
{
    public static class TestData
    {
        public static string UserName
        {
            get
            {
                return "C001";
            }
        }

        public static string Password
        {
            get
            {
                return "12345678";
            }
        }

        public static string FireFoxWebDriverLocation
        {
            get
            {
                return "C:\\Users\\Fletcher\\Downloads\\geckodriver-v0.20.0-win64";
            }
        }

        public static string BaseUrl
        {
            get
            {
                return "http://flask-env.jrngek2xcp.ap-southeast-2.elasticbeanstalk.com/";
            }
        }
    }
}