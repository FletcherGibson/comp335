﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;

namespace SelDemo
{
    public class LoginPage
    {
        [FindsBy(How = How.Name, Using = "username")]
        private IWebElement userNameTextField;

        [FindsBy(How = How.Name, Using = "password")]
        private IWebElement passwordTextField;

        [FindsBy(How = How.ClassName, Using = "login_button")]
        private IWebElement logInButton;

        public void Goto()
        {
            Browser.Initialize();
            Browser.Goto("login");
        }


        public void LogInUserAs(string user, string password)
        {

            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            userNameTextField.SendKeys(user);
            passwordTextField.SendKeys(password);
            logInButton.Click();
        }
    }
}