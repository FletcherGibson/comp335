﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelDemo.Pages
{
    class TenantPage
    {
        public void Goto()
        {
            Browser.Initialize();
            Browser.Goto("/tenant/login");
        }
    }
}
