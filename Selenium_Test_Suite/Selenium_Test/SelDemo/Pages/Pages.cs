﻿using SelDemo;
using OpenQA.Selenium.Support.PageObjects;
using SelDemo.Pages;

namespace SelDemo
{
    public static class Pages
    {
        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(Browser.Driver, page);
            return page;
        }

        public static LoginPage LoginPage
        {
            get { return GetPage<LoginPage>(); }
        }

        public static AboutPage AboutPage

        {
            get { return GetPage<AboutPage>(); }
        }

        public static TenantPage TenantPage

        {
            get { return GetPage<TenantPage>(); }
        }

    }
}