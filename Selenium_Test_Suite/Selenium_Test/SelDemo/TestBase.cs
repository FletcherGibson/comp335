﻿using System;
using NUnit.Framework;

namespace SelDemo
{
    [TestFixture]
    public class TestBase
    {
        [OneTimeSetUp]
        public static void Initialize()
        {
            Browser.Initialize();
        }

        [OneTimeTearDown]
        public static void OneTimeTearDown()
        {
            Browser.Close();
        }
    }
}
