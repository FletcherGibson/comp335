﻿using SelDemo;
using NUnit.Framework;
using System;
using SelDemoTests.TestDataAccess;

namespace SelDemo
{
    [TestFixture]
    public class Login : TestBase
    {
        [Test]
        public void CanLoginToApplication()
        {
            Pages.LoginPage.Goto();
            Pages.LoginPage.LogInUserAs(TestData.UserName, TestData.Password);
        }

        [Test]
        public void CanNotLoginWithInvalidUserName()
        {
            Pages.LoginPage.Goto();
            Pages.LoginPage.LogInUserAs(TestData.UserName, TestData.Password);
        }

        [Test]
        public void CanNotLoginWithInvalidPassword()
        {
            Pages.LoginPage.Goto();
            Pages.LoginPage.LogInUserAs(TestData.UserName, TestData.Password);
        }

        [Test]
        public void CanNotLoginWithInvalidUserNameAndPassword()
        {
            Pages.LoginPage.Goto();
            Pages.LoginPage.LogInUserAs(TestData.UserName, TestData.Password);
        }
    }
}
