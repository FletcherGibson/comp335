package comp335.cinemapossystem;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HomeControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void canReachHome() throws Exception {
		mockMvc.perform(get("/"))
		.andExpect(status().isOk())
		.andExpect(view().name("index/login"));
	}
	
	@Test
	public void canReachLoginPage() throws Exception {
		mockMvc.perform(get("/login"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canSubmitLogin() throws Exception {
		mockMvc.perform(get("/submitlogin"))
		.andExpect(status().isOk())
		.andExpect(view().name("redirect:/login"));
	}
	
	@Test
	public void canReachLogout() throws Exception {
		mockMvc.perform(get("/logout"))
		.andExpect(status().isOk())
		.andExpect(view().name("redirect:/"));
	}
	
}
