package comp335.cinemapossystem;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TenantControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void canReachTenantLogin() throws Exception {
		mockMvc.perform(get("/tenant/login"))
		.andExpect(status().isOk())
		.andExpect(view().name("tenantLogin"));
	}
	
	@Test
	public void canReachTenantFeatures() throws Exception {
		mockMvc.perform(get("/tenant/selectfeatures"))
		.andExpect(status().isOk())
		.andExpect(view().name("tenant/select_features"));
	}
}
