package comp335.cinemapossystem;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void canAccessUser() throws Exception {
		mockMvc.perform(get("/user/login"))
		.andExpect(status().isOk())
		.andExpect(view().name("/user/userLogin"));
	}
	
	@Test
	public void canReturnUser() throws Exception {
		mockMvc.perform(get("/user/create"))
		.andExpect(status().isOk())
		.andExpect(view().name("/user/result"));
	}
	
	
}
