package comp335.cinemapossystem;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void canAccessAdminLogin() throws Exception {
		mockMvc.perform(get("/admin/login"))
		.andExpect(status().isOk())
		.andExpect(view().name("/admin/adminLogin"));
	}
	
	@Test
	public void canReachAdminServerInfo() throws Exception {
		mockMvc.perform(get("/admin/serverinfo"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachAdminTenantInfo() throws Exception {
		mockMvc.perform(get("/admin/tenantinfo"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachAdminFeatureInfo() throws Exception {
		mockMvc.perform(get("/admin/featureinfo"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canREachActiveFeatures() throws Exception {
		mockMvc.perform(get("/activefeatures"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachAdminActiveFeatureInfo() throws Exception {
		mockMvc.perform(get("/admin/activefeatureinfo"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachGetServerInfo() throws Exception {
		mockMvc.perform(get("/getServerInfo"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachGetAllFeatures() throws Exception {
		mockMvc.perform(get("/allfeatures"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void canReachGetAllPermiddions() throws Exception {
		mockMvc.perform(get("/allpermissions"))
		.andExpect(status().isOk());
	}
	
}


