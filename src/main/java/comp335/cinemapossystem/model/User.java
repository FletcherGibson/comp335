package comp335.cinemapossystem.model;

public class User {
	
	public int id;
	public String username;
	public String password;
	public int tenantId;
	
	public User() {}
	
	public User(int id, String username, String password, int tenantId) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.tenantId = tenantId;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	} 

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

}
