package comp335.cinemapossystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class comp335cinemapossystem {

	public static void main(String[] args) {
		SpringApplication.run(comp335cinemapossystem.class, args);
	}
}
