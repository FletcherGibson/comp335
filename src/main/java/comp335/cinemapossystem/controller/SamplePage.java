package comp335.cinemapossystem.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class SamplePage {
	
	
	@RequestMapping(value="/sample2/{filename}", method=RequestMethod.GET)
	public String serveSample2(@PathVariable(value = "filename") String filename) {   
		return "redirect:/sample2/redirect";
	}	
	
	@RequestMapping(value="/sample2/redirect", method=RequestMethod.GET)
	public String redirected(@PathVariable(value = "filename") String filename) {   
		return "sample2/index";
	}
	
	@RequestMapping(value="/sample/{filename}", method=RequestMethod.GET)
	public String serveSample(@PathVariable(value = "filename") String filename) {   
		return "sample/"+filename;
	}
	
	@RequestMapping(value="/samplepermission", method=RequestMethod.GET)
	@ResponseBody
	public String[] samplePermissions(){   
		
		String[] sample_data = new String[]{
                "search system",
                "online payment",
                "feature 3",
                "feature 4",
                "feature 5", 
                "feature 6",
                "feature 7",
                "feature 8",
                "feature 9",
                "feature 10"
		};
		return sample_data;
	}	

	
}
