package comp335.cinemapossystem.controller;


import comp335.cinemapossystem.controller.DBController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TenantController {
	
	@RequestMapping ("/tenant/login")
	public String tenantLogin() {
		return "tenantLogin";
	}
	
	@RequestMapping (value="/tenant/selectfeatures", method=RequestMethod.GET)
	public String tenantSelectFeatures() {
		return "tenant/select_features";
	}
	
	@RequestMapping (value="/tenant/getPermissions", method=RequestMethod.GET)
	@ResponseBody
	public List<String> getPermissions(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		DBController db = new DBController();
		List<String> perm_list = new ArrayList<String>();
		perm_list.addAll(db.tenantGetPermissions(session_id));
		
		return perm_list;
	}
	
	@RequestMapping (value="/tenant/submitDesign", method=RequestMethod.POST)
	public String tenantSubmitDesign(@CookieValue(value="session_id", defaultValue="no_user") String session_id,
			@RequestParam("selectedFt[]") String[] selectedFt,
			@RequestParam("permission_data[]") String[] availFt) {
		
		DBController db = new DBController();
		db.updateActiveFeatures(session_id, selectedFt, availFt);
		
		return "redirect:/tenant/selectfeatures";
	}
}
 