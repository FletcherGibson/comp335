package comp335.cinemapossystem.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBController {
	private final String mysqljdbc = "jdbc:mysql://";
	private final String dburl = "comp335-mysql.cb1zkx0jmbwk.us-west-2.rds.amazonaws.com";
	private final String port = ":3306/";
	private final String dbName = "system";
	private final String username = "comp335";
	private final String password = "12345678";	
	private final String url = mysqljdbc + dburl + port + dbName;
	Connection conn;
	Statement query;
	
	public DBController() {
		try {
			// setup driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// setup connection
			conn = DriverManager.getConnection (url, username, password);
			// create query handler
			query = conn.createStatement();
		}	catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Queries database for valid login. Returns the user ID and name or null
	 * @param username
	 * @param password
	 * @return
	 */
	public List<String> checkLoginInfo(String username, String password) {
		List<String> sessionInfo = new ArrayList<String>();
		String sqlQuery = "SELECT tenant_name, tenant_id "
				+ "FROM tenant "
				+ "WHERE tenant_name=? AND password=?;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, username);
			p.setString(2, password);
			
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				sessionInfo.add(queryRes.getString("tenant_id"));
				sessionInfo.add(queryRes.getString("tenant_name"));
			}
			return sessionInfo;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * Gets a complete list of tenant permissions to use features 
	 * @return
	 */
	public List<String[]> getAllPermissions() {   	
		List<String[]> full_info = new ArrayList<String[]>();
		String sqlQuery = "SELECT * FROM featureflag ORDER BY featurename, tenantid;";

		try {
			ResultSet queryRes = query.executeQuery(sqlQuery);
			while (queryRes.next()) {
				String name = queryRes.getString("tenantid");
				String feature = queryRes.getString("featurename");
				String[] dataset = {name, feature};
				full_info.add(dataset);
			}
		}  catch (SQLException e) {
			e.printStackTrace();
		}
		
		return full_info;
	}
	
	
	/**
	 * Gets a list of features that the tenant has access to
	 * @param tenant_id
	 * @return
	 */
	public List<String> tenantGetPermissions(String tenant_id) {   	
		List<String> returnInfo = new ArrayList<String>();
		String sqlQuery = "SELECT featurename FROM featureflag WHERE tenantid=? ORDER BY featurename;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, tenant_id);
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				returnInfo.add(queryRes.getString("featurename"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnInfo;
	}

	/**
	 * Updates database with list of feature tenant is currently using
	 * @param session_id
	 * @param selectedFt
	 * @param availFt0
	 */
	public void updateActiveFeatures(String session_id, String[] selectedFt, String[] availFt) {	
		// TODO no longer need availFt
		try {
			// Delete old list of selected features
			String sqlQuery = "DELETE FROM active_features WHERE tenantid = ?;";
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, session_id);
			p.executeUpdate();
			
			// Update new selection
			for (int i=0; i<selectedFt.length; i++) {
				sqlQuery = "INSERT INTO active_features (tenantid, featurename) VALUES (?, ?)";
				p = conn.prepareStatement(sqlQuery);
				p.setString(1, session_id);
				p.setString(2,  selectedFt[i]);		
				p.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a list of all features offered
	 * @return List<String>
	 */
	public List<String> getFeatureList() {
		List<String> returnInfo = new ArrayList<String>();
		String sqlQuery = "SELECT featurename FROM featureflag ORDER BY featurename;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				returnInfo.add(queryRes.getString("featurename"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnInfo;
	}


	
	/**
	 * Returns a list active features and their associated tenant
	 * @return List<String[]>
	 */
	public List<String[]> getActivefeatures() {
		List<String[]> returnInfo = new ArrayList<String[]>();
		String sqlQuery = "SELECT tenantid, featurename FROM active_features ORDER BY tenantid, featurename;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				String dataPair[] = {queryRes.getString("tenantid"), queryRes.getString("featurename")};
				returnInfo.add(dataPair);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnInfo;
	}

	/**
	 * 
	 * @return
	 */
	public List<String[]> getServerInfo() {
		List<String[]> returnInfo = new ArrayList<String[]>();
		
		// TODO update to correct SQL query
		String sqlQuery = "SELECT featurename FROM featureflag ORDER BY featurename;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				// TODO extract data from database return
				// returnInfo.addAll(queryRes.get_();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnInfo;
	}

	/**
	 * Returns a list of all tenants along with non-sensitive information
	 * @return List<String[]>
	 */
	public List<String[]> getTenants() {
		List<String[]> tenantList = new ArrayList<String[]>();
		String sqlQuery = "SELECT tenant_id, tenant_name FROM tenant ORDER BY tenant_id;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			ResultSet queryRes = p.executeQuery();
			while (queryRes.next()) {
				String tenantData[] = {queryRes.getString("tenant_id"), queryRes.getString("tenant_name")};
				tenantList.add(tenantData);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tenantList;
	}


	public boolean disableFeature(String tenantid, String featurename) {
		// TODO Auto-generated method stub
		String sqlQuery = "DELETE FROM active_features WHERE tenantid = ? AND featurename = ?;";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, tenantid);
			p.setString(2, featurename);
			p.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public boolean enableFeature(String tenantid, String featurename) {
		// TODO Auto-generated method stub
		String sqlQuery = "INSERT INTO active_features VALUES(?, ?);";
		try {
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, tenantid);
			p.setString(2, featurename);
			p.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	

	public List<String[]> getTenantFeatures(String tenantid) {
		// TODO Update remote database to handle feature status
		try {
			
			// get list of active features
			String sqlQuery = "SELECT featurename FROM active_features WHERE tenantid=? ORDER BY tenantid, featurename;";
			List<String> active = new ArrayList<String>();
			// pass parameters to SQL
			PreparedStatement p = conn.prepareStatement(sqlQuery);
			p.setString(1, tenantid);
			ResultSet queryRes1 = p.executeQuery();
			while (queryRes1.next()) {
				active.add(queryRes1.getString("featurename"));
			}

			// get list of all features available to tenant
			sqlQuery = "SELECT featurename FROM featureflag WHERE tenantid=? ORDER BY tenantid, featurename;";
			List<String> permission = new ArrayList<String>();
			p = conn.prepareStatement(sqlQuery);
			p.setString(1, tenantid);
			p.executeQuery();
			ResultSet queryRes2 = p.executeQuery();
			while (queryRes2.next()) {
				permission.add(queryRes2.getString("featurename"));
			}
			
			// compare data and append status
			List<String[]> mergedInfo = new ArrayList<String[]>();
			
			for (int i=0; i<permission.size(); i++) {
				String featureStatus = "inactive";
				if (active.contains(permission.get(i))) {
					featureStatus = "active";
				}				
				String featureData[] = {tenantid, permission.get(i), featureStatus};
				mergedInfo.add(featureData);
			}
			
			return mergedInfo;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}



	
	

}
