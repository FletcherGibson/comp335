package comp335.cinemapossystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminController {

	@RequestMapping("/admin/login")
	public String adminLogin() {
		return "admin/adminLogin";
	}
	
	
	@RequestMapping("/admin")
	public String adminHome(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			return "admin/admin_page";
		}
		return null;
	}
	
	
	/***********	site map for administrator views	***********/
	
	
	@RequestMapping(value="/admin/serverinfo", method=RequestMethod.GET)
	public String serverinfo(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			// TODO url mapping stub
			return "admin/admin_server_info";
		}
		return null;
	}
	
	
	@RequestMapping(value="/admin/tenantinfo", method=RequestMethod.GET)
	public String tenantinfo(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			// TODO url mapping stub
			return "admin/admin_tenant";
		}
		return null;
	}
	
	
	@RequestMapping(value="/admin/featureinfo", method=RequestMethod.GET)
	public String featureinfo(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			// TODO url mapping stub
			return "admin/admin_featurelist";
		}
		return null;
	}
	
	
	@RequestMapping(value="/admin/activefeaturesinfo", method=RequestMethod.GET)
	public String activefeatureinfo(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			// TODO url mapping stub
			return "admin/admin_activefeatures";
		}
		return null;
	}

	
	
	/***********	site map for administrator data retrieval	***********/
	
	@RequestMapping(value="/alltenants", method=RequestMethod.GET)
	@ResponseBody
	private List<String[]> alltenants(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			List<String[]> data = new ArrayList<String[]>();
			DBController db = new DBController();
			data.addAll(db.getTenants());
			return data;
		}
		return null;
	}
	
	@RequestMapping(value="/activefeatures", method=RequestMethod.GET)
	@ResponseBody
	private List<String[]> getActivefeatures(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			List<String[]> data = new ArrayList<String[]>();
			DBController db = new DBController();
			data.addAll(db.getActivefeatures());
			return data;
		}
		return null;
	}

	@RequestMapping(value="/getserverinfo", method=RequestMethod.GET)
	@ResponseBody
	private List<String[]> getServerInfo(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			List<String[]> data = new ArrayList<String[]>();
			DBController db = new DBController();
			data.addAll(db.getServerInfo());
			return data;
		}
		return null;
	}
	
	@RequestMapping(value="/allfeatures", method=RequestMethod.GET)
	@ResponseBody
	private List<String> getFeatureList(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		if (checkAmAdmin(session_id)) {
			List<String> data = new ArrayList<String>();
			DBController db = new DBController();
			data.addAll(db.getFeatureList());
			return data;
		}
		return null;
	}
	
	@RequestMapping(value="/allpermissions", method=RequestMethod.GET)
	@ResponseBody
	private List<String[]> getPermissionList(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {   	
		if (checkAmAdmin(session_id)) {
			List<String[]> data = new ArrayList<String[]>();
			DBController db = new DBController();
			data.addAll(db.getAllPermissions());
			return data;
		}
		return null;
	}
	
	@RequestMapping(value="/admin/viewTenantFeatures", method=RequestMethod.POST)
	@ResponseBody
	private List<String[]> viewTenantFeatures(@CookieValue(value="session_id", defaultValue="no_user") String session_id,
			@RequestParam("tenantInfo[]") String[] tenantInfo)
	{		
		if (checkAmAdmin(session_id)) {
			DBController db = new DBController();
			return db.getTenantFeatures(tenantInfo[1]);
		}
		return null;
	}
	
	
	/***********	site map for administrator action	***********/
	@RequestMapping(value="/admin/disablefeature", method=RequestMethod.POST)
	@ResponseBody
	private boolean disableFeature(@CookieValue(value="session_id", defaultValue="no_user") String session_id,
			@RequestParam("disableInfo[]") String[] disableInfo)
	{		
		if (checkAmAdmin(session_id)) {
			DBController db = new DBController();
			return db.disableFeature(disableInfo[1], disableInfo[2]);
		}
		return false;
	}
	
	@RequestMapping(value="/admin/enablefeature", method=RequestMethod.POST)
	@ResponseBody
	private boolean enableFeature(@CookieValue(value="session_id", defaultValue="no_user") String session_id,
			@RequestParam("enableInfo[]") String[] enableInfo)
	{		
		if (checkAmAdmin(session_id)) {
			DBController db = new DBController();
			return db.enableFeature(enableInfo[1], enableInfo[2]);
		}
		return false;
	}
	

	
	/**
	 * Validates user for administrator access
	 * @param session_id
	 * @return
	 */
	private boolean checkAmAdmin(String session_id) {
		if (session_id.charAt(0) == 'a') {
			return true;
		}
		return false;
	}
}
