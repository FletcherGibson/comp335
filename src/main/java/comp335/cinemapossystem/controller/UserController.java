package comp335.cinemapossystem.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import comp335.cinemapossystem.model.User;

@Controller
public class UserController {

	@RequestMapping("/user/login")
	public String loginOrCreate() {
		return "/user/userLogin";	
	}
	
	  @GetMapping("/user/create")
	    public String userForm(Model model) {
	        model.addAttribute("user", new User());
	        return "/user/greeting";
	    }
	  
	  @PostMapping("/user/create")
	    public String userSubmit(@ModelAttribute User user) {
	        return "/user/result";
	    }
	
//	@RequestMapping(value="/user/create", method=RequestMethod.GET)
//	public String user(@RequestParam ("username") String username ,
//					   @RequestParam ("password") String password,
//					   @RequestParam ("tenantId") int tenantId, Model model) {
//		User u = new User();
//		u.setUsername(username);
//		u.setPassword(password);
//		u.setTenantId(tenantId);
//		model.addAttribute("user",u);
//		return "user";
//	}
}
