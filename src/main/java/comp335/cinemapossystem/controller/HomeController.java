package comp335.cinemapossystem.controller;

import comp335.cinemapossystem.controller.DBController;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
	
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		
		// check if logged in
		if (session_id.equals("no_user")) {
			return "redirect:/login";
		}
		char user_type = session_id.charAt(0);
		if (user_type == 'a') {
			return "redirect:/admin/admin_page";
		} else if (user_type == 't') {
			return "redirect:/tenant/selectfeatures";
		} else if (user_type == 'c') {
			return "redirect:/user/greeting";
		}
		
		
		return "home/index";		// default/fallback home page
	}
	
	/**
	 * Checks for a valid session
	 * @param session_id
	 * @param path
	 * @return user_type or null {a:admin, c:client, t:tenant}
	 */
	@SuppressWarnings("unused")
	private String checkSession(String session_id) {
		// TODO make sure session belongs to a valid user in the database
		if (session_id.equals("no_user")) {
			return "null";
		}
		return "valid";
	}
	
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String loginPage(@CookieValue(value="session_id", defaultValue="no_user") String session_id) {
		// if the user is already logged in, send them to the home page
		if (session_id.equals("no_user")) {
			return "home/login2";	// return the login page, template #2
		}
		return "redirect:/";
	}
	
	
	/**
	 * 
	 * @param session_id
	 * @param username
	 * @param password
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/submitlogin", method=RequestMethod.POST)
	public String loginP(@CookieValue(value="session_id", defaultValue="no_user") String session_id,
							@RequestParam("username") String username,
							@RequestParam("password") String password,
							HttpServletResponse response) 
	{
		/*
		int i=0;
		int j=2;
		if (i<j) {return "redirect:/tenant/selectfeatures";}
		*/
		if (session_id.equals("no_user")) {		// check that the user is not already logged in
			DBController db = new DBController();
			List<String> info = new ArrayList<String>();
			info = db.checkLoginInfo(username, password);
			// if return data is empty, then login failed
			// TODO notify user login failed
			if (info.size() == 0) {
				return "redirect:/login";
			}
						
			// login is successful, generate a session cookie
			Cookie session_cookie = new Cookie("session_id", info.get(0)); 
			session_cookie.setMaxAge(99999999); 	//set expire time 
			response.addCookie(session_cookie); 	//put cookie in response 
			
			session_cookie = new Cookie("user", info.get(1)); 
			session_cookie.setMaxAge(99999999); 	//set expire time 
			response.addCookie(session_cookie); 	//put cookie in response 
			
			// check account ID and log them into appropriate page
			if (info.get(0).charAt(0) =='t') {
				return "redirect:/tenant/selectfeatures";
			} else if ((info.get(0).charAt(0) =='a')) {
				return "redirect:/admin";
			}
		}
		// if the user is already logged in, send them to the home page
		return "redirect:/";
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletResponse response) {	
		// create cookies of identical fields, set them to expire immediately
		Cookie session_cookie = new Cookie("session_id", null); 
		session_cookie.setMaxAge(0);
		response.addCookie(session_cookie);
		
		session_cookie = new Cookie("user", null); 
		session_cookie.setMaxAge(0);
		response.addCookie(session_cookie);
		
		return "redirect:/";
	}
	

} 
 