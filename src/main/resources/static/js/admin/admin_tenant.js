(function(){
	
	var specificTenantViewData = [];
	
	(function() {loadPage()})();
	
    function loadPage() {
    	console.log("/js/admin/admin_tenant.js")
        $.get("/alltenants", function(data) {
        	var tablehtml = ""
        		+ "<tr>"
        		+		"<th>Tenant ID</th>"
        		+		"<th>Tenant Name</th>"
        		+		"<th>View Feature Information</th>"
        		+ "</tr>";
        	
        	for (i=0; i<data.length; i++) {
        		var tenantid = data[i][0];
        		var tenantName = data[i][1];
        		tablehtml += ""
            		+ "<tr>"
            		+		"<th>" + tenantid + "</th>"
            		+		"<th>" + tenantName + "</th>"
            		+ 		"<th>"
			        +    		"<form action='/admin/viewTenantFeatures' method='get'>"
			        +				"<button class='viewTenantInfo' type='submit' id='featureInfo_" + tenantid + "_" + tenantName + "' formaction='/admin/viewTenantFeatures'>View Feature Information</button>"
			        +    		"</form>"            		
            		+		"</th>"
            		+ "</tr>";
        	}
        	
        	$("#content").html(tablehtml);
        });
    };
    
    
    $("#content").on("click", ".viewTenantInfo", function(event) {
    	event.preventDefault();
    	
    	var tenantInfo = event.target.id.split("_");
	    $.ajax({
	        type:   "POST",
	        url:    "/admin/viewTenantFeatures",
	        data:  {"tenantInfo":tenantInfo},
	        success: function(data) {
	        	displayTenantFeatureStatus(data, event.target.id.substring(17, 24))
	        },
	    });
	    console.log("\nclick viewTenantInfo")
	    console.log(event.target.id)
    }); 


	function displayTenantFeatureStatus(data, tenantname) {
    	var tablehtml = ""
	        + "<form>"
	        +		"<button type='submit' id='back'>Back</button>"
	        + "</form>" 
    		+ "<h3>" + tenantname + "</h3>"
    		+ "<table><tr>"
    		+		"<th>Feature Name</th>"
    		+		"<th>Status</th>"
    		+		"<th>Action</th>"
    		+ "</tr>";
    	
    	
    	for (i=0; i<data.length; i++) {
    		var tenantid = data[i][0];
    		var featurename = data[i][1];
    		var featurestatus = data[i][2];
    		tablehtml += ""
        		+ "<tr>"
        		+		"<th>" + featurename + "</th>"
        		+		"<th>" + featurestatus + "</th>";
    		
    		if (featurestatus === "active") {
    			tablehtml += ""
            		+ 		"<th>"
			        +    		"<form action='/admin/disablefeature' method='get'>"
			        +    			"<button class='disablefeature' type='submit' id='disable_" + tenantid + "_" + featurename + "_" + tenantname + "' formaction='/admin/disablefeature'>Disable</button>"
			        +    		"</form>"            		
            		+		"</th>";
    		} else if (featurestatus === "inactive") {
    			tablehtml += ""
            		+ 		"<th>"
			        +    		"<form action='/admin/disablefeature' method='get'>"
			        +    			"<button class='enablefeature' type='submit' id='enabler_" + tenantid + "_" + featurename + "_" + tenantname + "' formaction='/admin/enablefeature'>Enable</button>"
			        +    		"</form>"            		
            		+		"</th>";
    		}
    		
    		
    		
        	tablehtml += "</tr>";
    	}
    	tablehtml += "</table>"
    	$("#content").html(tablehtml);
	}
	
	
	
    $("#content").on("click", "#back", function(event) {
    	event.preventDefault();
    	loadPage();
    }); 
    
    
    // TODO enable and disable refreshes the display
    $("#content").on("click", ".disablefeature", function(event) {
    	event.preventDefault();
    	var disableInfo = event.target.id.split("_");
	    $.ajax({
	        type:   "POST",
	        url:    "/admin/disablefeature",
	        data:  {"disableInfo":disableInfo},
	    });

    });    
    
    
    $("#content").on("click", ".enablefeature", function(event) {
    	event.preventDefault();
    	var enableInfo = event.target.id.split("_");
	    $.ajax({
	        type:   "POST",
	        url:    "/admin/enablefeature",
	        data:  {"enableInfo":enableInfo},
	    });    	
    });   

})()