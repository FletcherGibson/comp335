(function(){
	/*
	 * TODO use templates in place of hard-coded HTML script and tags
	 */

    (function() {
    	console.log("activefeatures.js")
        $.get("/activefeatures", function(data) {
        	var tablehtml = ""
        		+ "<tr>"
        		+		"<th>Tenant ID</th>"
        		+		"<th>Feature Name</th>"
        		+		"<th>Disable </th>"
        		+ "</tr>";
        	
        	for (i=0; i<data.length; i++) {
        		var tenantid = data[i][0];
        		var ftname = data[i][1];
        		tablehtml += ""
            		+ "<tr>"
            		+		"<th>" + tenantid + "</th>"
            		+		"<th>" + ftname + "</th>"
            		+ 		"<th>"
			        +    		"<form action='/admin/disablefeature' method='get'>"
			        +    			"<button class='disablefeature' type='submit' id='disable_" + tenantid + "_" + ftname + "' formaction='/admin/disablefeature'>Disable</button>"
			        +    		"</form>"            		
            		+		"</th>"
            		+ "</tr>";
        	}
        	for (i=0; i<5; i++) {
            	tablehtml += ""
            		+ "<tr>"
            		+		"<th> </th>"
            		+		"<th> </th>"
            		+		"<th></th>"
            		+ "</tr>";
        	}

        	$("#content").html(tablehtml);
        	console.log(data)
        });
    })();
    
    
    $("#content").on("click", ".disablefeature", function(event) {
    	event.preventDefault();
    	var disableInfo = event.target.id.split("_");
	    $.ajax({
	        type:   "POST",
	        url:    "/admin/disablefeature",
	        data:  {"disableInfo":disableInfo},
	        dataType: "String",
	        success: location.reload()
	    });
    	
    });    

})()


