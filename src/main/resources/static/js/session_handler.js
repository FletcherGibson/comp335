(function(){
	
	
	var session = document.cookie;
	var userName = getCookie("user");
	var userID = getCookie("session_id");

	(function checkLoggedIn(){
		if (userName && userID){
			var btn_html = "" 
				+ "<li><label for='logoutbtn' >Logged in as: " + userName + "</label></li>"
				+ "<li><form id='logoutbtn' action='/logout'>" 
				+ 		"<button class='logout' type='submit' value='Logout'>Logout</button>" 
				+ "</form></li>";
			$("#session").html(btn_html)
		}
	}());
	
	
	
	
	// https://www.w3schools.com/Js/js_cookies.asp
	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}	
	
})()