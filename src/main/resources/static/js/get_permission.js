(function(){

    var permission_data = [];

    (function get_permissions() {
        permission_data = [];
        // get list of tenant permissions from url
        $.get("/tenant/getPermissions", function(data) {
        	permission_data = data;
            load_feature_selection()
            load_sample_preview()
        });
    })();

    function load_feature_selection() {
    	// TODO use template compatible with JSP MVC
    	var html = "";
    	
    	
    	// load checkbox for each feature	
    	for (i=0; i<permission_data.length; i++) {
            var ft_list = "" 
            	+ "\n<li><input type='checkbox' name='feature_permission' value='" 
            	+ permission_data[i]
            	+"'>"
            	+ permission_data[i]
            	+ "</li>";
            html +=ft_list
    	}
        $("#features").html(html)
    }

    
    function load_sample_preview() {
        $.get("/sample/index.html", function(data) {
            $("#preview_pane").html(data)
        });
    }


    $("#feature_selection_pane").on("click", "#save_design", function(event){
        event.preventDefault();
        //TODO send feature selection to server
	    var selectedFt = []
	    $('input[type=checkbox]:checked').each(function(i , event){
	    	selectedFt.push( $(event).attr('value') )
	    })
	    
	    $.ajax({
	        type:   "POST",
	        url:    "/tenant/submitDesign",
	        data:  {"selectedFt[]":selectedFt, "permission_data[]":permission_data},
	    });
	    
	    alert("Your changes have been saved")
    });

    /*
    * Reloads only the preview pane when navigating between links/pages
    */
    $("#preview_pane").on("click", "a", function(event) {
        event.preventDefault();
        $.get(event.target.href, function(data) {
            $("#preview_pane").html(data)
        });
    });

})()